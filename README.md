This is my parallel algorithm to test chordality of graphs.
It uses the parallel LexBFS algorithm with partition 
refinment technique.

Copyright © 2016 Agnieszka Lupinska
agnieszka.lupinska@uj.edu.pl

Testing Chordal Graphs with CUDA
GPU Technology Conference, Silicon Valley, California, April 4-7, 2016

This program is provided for scientific and educational only.
