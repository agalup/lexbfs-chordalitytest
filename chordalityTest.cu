// Copyright © 2016 Agnieszka Lupinska
// agnieszka.lupinska@uj.edu.pl
//
// Testing Chordal Graphs with CUDA
// GPU Technology Conference, Silicon Valley, California, April 4-7, 2016
//
// This is my parallel algorithm to test chordality of graphs.
// It uses the parallel LexBFS algorithm with partition 
// refinment technique.
//
// This program is provided for scientific and educational only.
//


 
#include "chordalityTest.a"

#define debug(a...) fprintf(stderr, a)

//#define debug1 debug
#define debug1

#define debugE debug
//#define debugE(a...) a

#define debugTime debug
//#define debugTime

double diffclock(clock_t clock1,clock_t clock2)
{
    double diffticks=clock1-clock2;
    double diffms=(diffticks*10)/CLOCKS_PER_SEC;
    return diffms;
}

bool chordalityTestGPU(){

    int result = 1;

    cudaMemcpy(d_Adj, Adj, aligned_N*aligned_N*sizeof(char), cudaMemcpyHostToDevice);

    set_memory_chordalityTest<<<blocks, threads>>>(aligned_N, d_leftAdj, d_current);
    cudaDeviceSynchronize();
    cudaError_t res = cudaGetLastError();
    if (res != 0)
        debugE("[%s][%d] %s\n", __FILE__, __LINE__, error(res));

    compute_LN_chordalityTest<<<blocks, threads>>>(aligned_N, d_Adj, d_order, d_leftAdj, d_right_neighbour);
    cudaDeviceSynchronize();
    res = cudaGetLastError();
    if (res != 0)
        debugE("[%s][%d] %s\n", __FILE__, __LINE__, error(res));

    chordalityTest<<<blocks, threads>>>(aligned_N, d_Adj, d_leftAdj, d_right_neighbour, d_current);
    cudaDeviceSynchronize();
    res = cudaGetLastError();
    if (res != 0)
        debugE("[%s][%d] %s\n", __FILE__, __LINE__, error(res));

    cudaMemcpy(&result, d_current, sizeof(int), cudaMemcpyDeviceToHost);
    cudaDeviceSynchronize();
    res = cudaGetLastError();
    if (res != 0)
        debugE("[%s][%d] %s\n", __FILE__, __LINE__, error(res));

    return result;
}

void initData(){
    Adj = (char*)malloc(aligned_N*aligned_N*sizeof(char));
    order = (int*)malloc((numberOfNodes+1)*sizeof(int));

    cudaError_t res = cudaMalloc((void**)&d_Adj,             aligned_N * aligned_N * sizeof(char));
    if (res != 0){
        debugE("[%s][%d] %s\n", __FILE__, __LINE__, error(res));
    }
    res = cudaMalloc((void**)&d_next_label,      aligned_N * aligned_N * sizeof(int));
    if (res != 0){
        debugE("[%s][%d] %s\n", __FILE__, __LINE__, error(res));
    }
    res = cudaMalloc((void**)&d_next_label_old,  aligned_N * sizeof(int));
    if (res != 0){
        debugE("[%s][%d] %s\n", __FILE__, __LINE__, error(res));
    }
    res = cudaMalloc((void**)&d_order,           aligned_N * sizeof(int));
    if (res != 0){
        debugE("[%s][%d] %s\n", __FILE__, __LINE__, error(res));
    }
    res = cudaMalloc((void**)&d_label,           aligned_N * sizeof(int));
    if (res != 0){
        debugE("[%s][%d] %s\n", __FILE__, __LINE__, error(res));
    }
    res = cudaMalloc((void**)&d_current,         sizeof(int));
    if (res != 0){
        debugE("[%s][%d] %s\n", __FILE__, __LINE__, error(res));
    }

    threads = dim3(THREADS);
    blocks = dim3((aligned_N/THREADS));
    initDataOK = true;
}
void freeMemory(){
    cudaFree(d_Adj);
    cudaFree(d_label);
    cudaFree(d_next_label);
    cudaFree(d_next_label_old);
    cudaFree(d_order);
    cudaFree(d_current);
    free(Adj);
    free(order);
}
void input(){
    fscanf(rf, "%d", &numberOfNodes);
    aligned_N = ceil((double)(numberOfNodes+1)/(double)THREADS)*THREADS;

    if (!initDataOK)
        initData();

    for (int i=0; i<aligned_N; ++i){
        for (int j=0; j<aligned_N; ++j){
            if (i == 0 || j == 0){
                Adj[(i * aligned_N) + j] = 0;
            }else{
                if (i <= numberOfNodes && j <= numberOfNodes){\
                    int newitem;\
                    fscanf(rf, "%d", &newitem);
                    Adj[(i * aligned_N) + j] = (char)newitem;\
                }else{\
                    Adj[(i * aligned_N) + j] = 0;\
                }
            }
        }
    }
    
    cudaMemcpy(d_Adj, Adj, aligned_N*aligned_N*sizeof(char), cudaMemcpyHostToDevice);
}
void LexBFS(){
    
    set_memory_lexBFS<<<blocks, threads>>>
        (numberOfNodes, aligned_N, d_label, d_next_label, d_current, d_order);

    cudaError_t resError;

    for (int time=0; time<numberOfNodes; ++time){

        set_counter_lexBFS<<<blocks, threads>>>
            (time, d_current, d_label, d_next_label, d_next_label_old, d_Adj, d_order, numberOfNodes);
        cudaDeviceSynchronize();\
        resError = cudaGetLastError();
        if (resError != 0){
            debugE("[%s][%d] %s\n", __FILE__, __LINE__, error(resError));
            break;
        }

        insert_newSet_lexBFS<<<blocks, threads>>>
            (aligned_N, d_current, d_Adj, d_label, d_next_label, d_next_label_old, numberOfNodes);
        cudaDeviceSynchronize();\
        resError = cudaGetLastError();
        if (resError != 0){
            debugE("[%s][%d] %s\n", __FILE__, __LINE__, error(resError));
            break;
        }

        move_to_newSet_lexBFS<<<blocks, threads>>>(aligned_N, d_current, d_Adj, d_label, d_next_label, d_Adj, 
                d_next_label_old, numberOfNodes);
        cudaDeviceSynchronize();\
        resError = cudaGetLastError();
        if (resError != 0){
            debugE("[%s][%d] %s\n", __FILE__, __LINE__, error(resError));
            break;
        }

        find_current_lexBFS<<<blocks, threads>>>(d_label, d_Adj, d_next_label, d_next_label_old, d_current, 
                numberOfNodes);
        cudaDeviceSynchronize();\
        resError = cudaGetLastError();           
        if (resError != 0){
            debugE("[%s][%d] %s\n", __FILE__, __LINE__, error(resError));
            break;
        }
    }

    cudaMemcpy(order, d_order, (numberOfNodes+1)*sizeof(int), cudaMemcpyDeviceToHost);
    cudaDeviceSynchronize();
    resError = cudaGetLastError();
    if (resError != 0){
        debugE("[%s][%d] %s\n", __FILE__, __LINE__, error(resError));
    }
}
void testCase(){
    clock_t startTime;

    if (!withoutInputTime){
        startTime = clock();
    }

    input();

    if (withoutInputTime){
        startTime = clock();
    }

    LexBFS(); 

    int res = chordalityTestGPU();

    if (res)
        printf("Is chordal\n");
    else
        printf("Is not chordal\n");

    clock_t endTime = clock();
    printf("%lf ms\n", (double)diffclock(endTime, startTime));

}
int main(int argc, char* argv[]){
    if (argc < 3){
        printf("Usage: ./chordalityTest <filename> <flag>\n\
\n\
<filename> is a test.\n\
The test is given in the following way:\n\
In the first line there is Z - the number of tested graphs\n\
In the second line there is N - the number of vertices of the first graph\n\
In the next N lines there is a adjacent matrix for the first graph\n\
If Z is grater than 1 in the next lines there are the other graphs\n\
given in the same way\n\
\n\
<flag> is a number.\n\
0 - to measure the running time of a program with the input and memory\n\
allocation, or \n\
1 - otherwise \n");
        return 1;
    }
    withoutInputTime = atoi(argv[2]);
    initDataOK = false;
    rf = fopen(argv[1], "r");
    int nrOfTests; fscanf(rf, "%d", &nrOfTests);
    for (int i=0; i<nrOfTests; ++i){
        printf("#test %d/%d\n", i+1, nrOfTests);
        testCase();
    }
    freeMemory();
    return 0;
}
const char * error(cudaError result){ 
    switch(result) { 
        case CUDA_SUCCESS: return "No errors";
        case CUDA_ERROR_INVALID_VALUE: return "Invalid value"; 
        case CUDA_ERROR_OUT_OF_MEMORY: return "Out of memory"; 
        case CUDA_ERROR_NOT_INITIALIZED: return "Driver not initialized"; 
        case CUDA_ERROR_DEINITIALIZED: return "Driver deinitialized"; 
                                       
        case CUDA_ERROR_NO_DEVICE: return "No CUDA-capable device available"; 
        case CUDA_ERROR_INVALID_DEVICE: return "Invalid device"; 
                                        
        case CUDA_ERROR_INVALID_IMAGE: return "Invalid kernel image"; 
        case CUDA_ERROR_INVALID_CONTEXT: return "Invalid context"; 
        case CUDA_ERROR_CONTEXT_ALREADY_CURRENT: return "Context already current"; 
        case CUDA_ERROR_MAP_FAILED: return "Map failed"; 
        case CUDA_ERROR_UNMAP_FAILED: return "Unmap failed"; 
        case CUDA_ERROR_ARRAY_IS_MAPPED: return "Array is mapped"; 
        case CUDA_ERROR_ALREADY_MAPPED: return "Already mapped"; 
        case CUDA_ERROR_NO_BINARY_FOR_GPU: return "No binary for GPU"; 
        case CUDA_ERROR_ALREADY_ACQUIRED: return "Already acquired"; 
        case CUDA_ERROR_NOT_MAPPED: return "Not mapped"; 
        case CUDA_ERROR_NOT_MAPPED_AS_ARRAY: return "Mapped resource not available for access as an array"; 
        case CUDA_ERROR_NOT_MAPPED_AS_POINTER: return "Mapped resource not available for access as a pointer"; 
        case CUDA_ERROR_ECC_UNCORRECTABLE: return "Uncorrectable ECC error detected"; 
        case CUDA_ERROR_UNSUPPORTED_LIMIT: return "CUlimit not supported by device";    
        case CUDA_ERROR_INVALID_SOURCE: return "Invalid source"; 
        case CUDA_ERROR_FILE_NOT_FOUND: return "File not found"; 
        case CUDA_ERROR_SHARED_OBJECT_SYMBOL_NOT_FOUND: return "Link to a shared object failed to resolve"; 
        case CUDA_ERROR_SHARED_OBJECT_INIT_FAILED: return "Shared object initialization failed";
        case CUDA_ERROR_INVALID_HANDLE: return "Invalid handle";                                
        case CUDA_ERROR_NOT_FOUND: return "Not found"; 
        case CUDA_ERROR_NOT_READY: return "CUDA not ready"; 
        case CUDA_ERROR_LAUNCH_FAILED: return "Launch failed"; 
        case CUDA_ERROR_LAUNCH_OUT_OF_RESOURCES: return "Launch exceeded resources"; 
        case CUDA_ERROR_LAUNCH_TIMEOUT: return "Launch exceeded timeout"; 
        case CUDA_ERROR_LAUNCH_INCOMPATIBLE_TEXTURING: return "Launch with incompatible texturing";
        case CUDA_ERROR_UNKNOWN: return "Unknown error";
        case cudaErrorInvalidValue: return "invalid value";
        case 9: return "too much threads in blocks";
        default: {
                     debugE("result = %d\n", result);
                     return "Unknown CUDA error value";
                 }
    } 
}
