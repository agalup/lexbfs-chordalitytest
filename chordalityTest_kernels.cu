// Copyright © 2016 Agnieszka Lupinska
// agnieszka.lupinska@uj.edu.pl
//
// Testing Chordal Graphs with CUDA
// GPU Technology Conference, Silicon Valley, California, April 4-7, 2016
//
// This is my parallel algorithm to test chordality of graphs.
// It uses the parallel LexBFS algorithm with partition 
// refinment technique.
//
// This program is provided for scientific and educational only.
//


__global__
void set_memory_lexBFS(int numberOfNodes, int aligned_N, int* label, int* next_label, int* current, int* order){
    int thid = (blockIdx.x * blockDim.x) + threadIdx.x;
    order[thid] = 0;
    for (int i = 0; i<aligned_N; ++i){
        next_label[(i * aligned_N) + thid] = 0;
    }
    if (thid < 2 || thid > numberOfNodes){
        label[thid] = 0;
    }else{
        label[thid] = 1;
    }
    if (thid == 1){
        next_label[1] = -1;
        current[0] = 1;
    }
}
__global__
void set_counter_lexBFS(int time, int* Item, int* label, int* next_label, 
        int* next_label_old, char* counter, int* order, int numberOfNodes){
    int thid = (blockIdx.x * blockDim.x) + threadIdx.x;
    if (thid > numberOfNodes)
        return;
    __shared__ int item; item = *Item;
    int l = label[thid];
    if (l > 0){
        next_label_old[thid] = next_label[l];
        counter[l] = 0x00;
    }
    if (thid == item){
        order[item] = time+1;     //vertexToLexOrder
//        order[time] = item;   //lexOrderToVertex
        label[item] = 0;
    }
}

__global__
void insert_newSet_lexBFS(int aligned_N, int* Item, char* Adj, int* label, int* next_label, 
        int* next_label_old, int numberOfNodes){

    __shared__ int item; item = *Item;
    int thid = (blockIdx.x * blockDim.x) + threadIdx.x;
    if (thid > numberOfNodes)
        return;
    int nextfree = (item*aligned_N)+thid;
    if (label[thid] <= 0 || Adj[nextfree] != 1){
        return;
    }
    int tmpLabel = label[thid];
    int tmpNextLabel = next_label_old[thid];

    if (next_label[tmpLabel] == tmpNextLabel){
        next_label[tmpLabel] = nextfree;
        next_label[nextfree] = tmpNextLabel;
        //if (atomicCAS(&next_label[tmpLabel], tmpNextLabel, nextfree) == tmpNextLabel){\
            next_label[nextfree] = tmpNextLabel;\
        }
    }
}
__global__
void move_to_newSet_lexBFS(int aligned_N, int* Item, char* Adj, int* label, int* next_label,  
        char* counter, int* next_label_old, int numberOfNodes){

    __shared__ int item; item = *Item;
    int thid = (blockIdx.x * blockDim.x) + threadIdx.x;
    if (thid > numberOfNodes)
        return;
    int nextfree = (item*aligned_N)+thid;
    int oldLabel = label[thid];
   
    if (label[thid] > 0 && Adj[nextfree] == 1){
        //update label
        label[thid] = next_label[oldLabel];
    }
    if (label[thid] > 0){
        counter[label[thid]] = 0xff;
        next_label_old[thid] = next_label[label[thid]];
    }
}
__global__
void find_current_lexBFS(int* label, char* counter, int* next_label, int* next_label_old, int* current,
        int numberOfNodes){
    int thid = (blockIdx.x * blockDim.x) + threadIdx.x;
    if (thid > numberOfNodes)
        return;
    int l = label[thid];
    int nl = next_label_old[thid];
    if (l > 0){
        if (nl > 0 && counter[nl] == 0x00){\
            next_label[l] = next_label[nl];\
        }
        if (next_label[l] == -1){
            *current = thid;
        }
    }
}
__global__
void set_memory_chordalityTest(int aligned_N, int* leftAdj, int* current){
    int thid = (blockIdx.x * blockDim.x) + threadIdx.x;
    if (thid == 0)
        current[0] = 1;
    for (int x = 0; x < aligned_N; ++x){
        leftAdj[(x * aligned_N) + thid] = 0;
    }
}
__global__
void compute_LN_chordalityTest(int aligned_N, char* Adj, int* vertexToLexOrder, int* leftAdj, int* right_neighbour){
    int thid = (blockIdx.x * blockDim.x) + threadIdx.x;
    int time_thid = vertexToLexOrder[thid];
    int right = 0, right_y = 0;
    for (int y = 1; y < aligned_N; ++y){
        if (Adj[(y * aligned_N) + thid]){
            int time_y = vertexToLexOrder[y];
            if (time_y < time_thid){
                leftAdj[(y * aligned_N) + thid] = 1;
                if (time_y > right){
                    right = time_y;
                    right_y = y;
                }
            }
        }
        leftAdj[(thid * aligned_N) + thid] = 1;
    }
    right_neighbour[thid] = right_y;
}
__global__
void chordalityTest(int aligned_N, char* Adj, int* leftAdj, int* right_neighbour, int* notChardinality){
    int thid = (blockIdx.x * blockDim.x) + threadIdx.x;
    int right = right_neighbour[thid];
    if (right == 0)
        return;
    for (int x = 1; x < aligned_N; ++x){
        if (x != thid && leftAdj[(x * aligned_N) + thid] && (! leftAdj[(x * aligned_N) + right])){
            *notChardinality = 0;
        }
    }
}




