NVCC := /usr/local/cuda/bin/nvcc

chordalityTest: chordalityTest.o chordalityTest_kernels.o
	$(NVCC) -o chordalityTest chordalityTest.o chordalityTest_kernels.o

chordalityTest.o: chordalityTest.cu chordalityTest_kernels.o
	$(NVCC) -c chordalityTest.cu

chordalityTest_kernels.o: chordalityTest_kernels.cu
	$(NVCC) -c chordalityTest_kernels.cu

clean:
	rm -f chordalityTest.o chordalityTest_kernels.o chordalityTest

#lexdfs: lexdfs.o lexdfs_kernels.o
#	$(NVCC) -o lexdfs lexdfs.o lexdfs_kernels.o

#lexdfs.o: lexdfs.cu lexdfs_kernels.o
#	$(NVCC) -c lexdfs.cu lexdfs_kernels.o

#lexdfs_kernels.o: lexdfs_kernels.cu
#	$(NVCC) -c lexdfs_kernels.cu


