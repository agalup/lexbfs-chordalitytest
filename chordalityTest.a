// Copyright © 2016 Agnieszka Lupinska
// agnieszka.lupinska@uj.edu.pl
//
// Testing Chordal Graphs with CUDA
// GPU Technology Conference, Silicon Valley, California, April 4-7, 2016
//
// This is my parallel algorithm to test chordality of graphs.
// It uses the parallel LexBFS algorithm with partition 
// refinment technique.
//
// This program is provided for scientific and educational only.
//

#ifndef _CHORDALITY_TEST_A_
#define _CHORDALITY_TEST_A_

#include<cstdio>
#include "cuda.h"
#include<cuda_profiler_api.h>
#include<vector>
#include<cmath>
#include<ctime>

/*#include<thrust/host_vector.h>
#include<thrust/device_vector.h>
#include<thrust/sequence.h>
#include<thrust/copy.h>
#include<thrust/fill.h>

using namespace thrust;
*/
using namespace std;

#define THREADS 1024
//cpu data
FILE *rf;
bool initDataOK;
bool withoutInputTime;
int numberOfNodes;
int aligned_N;
dim3 blocks, blocks2;
dim3 threads, threads2;
char* Adj;
int* order;
//gpu data
char *d_Adj;
int *d_next_label, *d_next_label_old, *d_label, *d_order, *d_current;

//chordalityTest
#define d_leftAdj           d_next_label
#define d_right_neighbour   d_next_label_old

const char * error(cudaError_t result);


__global__
void set_memory_lexBFS(int, int, int*, int*, int*, int*);

__global__
void set_counter_lexBFS(int, int*, int*, int*, int*, char*, int*, int);

__global__
void insert_newSet_lexBFS(int, int*, char*, int*, int*, int*, int);

__global__
void move_to_newSet_lexBFS(int, int*, char*, int*, int*, char*, int*, int);

__global__
void find_current_lexBFS(int*, char*, int*, int*, int*, int);

__global__
void set_memory_chordalityTest(int, int*, int*);

__global__
void compute_LN_chordalityTest(int, char*, int*, int*, int*);

__global__
void chordalityTest(int, char*, int*, int*, int*);

#endif
